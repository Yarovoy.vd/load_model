use core::fmt;
use std::{iter, time};

use tracing::debug_span;

pub type GeneratorIter<T> = Box<dyn Iterator<Item = T> + Send>;

pub fn generator_counter() -> GeneratorIter<usize> {
    let mut counter = 0;
    Box::new(iter::from_fn(move || {
        counter += 1;
        Some(counter)
    }))
}

#[derive(Clone)]
pub struct TaskMessage {
    id: usize,
    generator_id: usize,
    created_at: time::Instant,
    message_life_span: tracing::Span,
}

impl fmt::Debug for TaskMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TaskMessage").field("id", &self.id).field("generator_id", &self.generator_id).field("created_at", &self.created_at).finish()
    }
}

impl fmt::Display for TaskMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TaskMessage").field("id", &self.id).field("generator_id", &self.generator_id).field("created_at", &self.created_at).finish()
    }
}

pub fn generator_message(generator_id: usize) -> GeneratorIter<TaskMessage> {
    let mut counter = 0;
    Box::new(iter::from_fn(move || {
        counter += 1;
        let created_at = time::Instant::now();
        Some(TaskMessage{
            id: counter,
            generator_id,
            created_at,
            message_life_span: debug_span!(target: "lifecycle_events", "Task message", message_id = counter, gen_id = generator_id, ?created_at)
        })
    }))
}

