use std::fmt::Debug;

use futures::{future, FutureExt};
use tokio::sync::mpsc::{Permit, Sender};
use tracing::{debug_span, info_span, instrument, Instrument};

pub struct OutputBalancer<T> {
    sinks: Box<[Sender<T>]>,
    last_sent_to_sink_idx: usize,
    output_balancer_span: tracing::Span,
}

impl<T> OutputBalancer<T> {
    #[instrument]
    pub fn new(sinks: Box<[Sender<T>]>, parent_span: &tracing::Span) -> Self {
        OutputBalancer {
            sinks,
            last_sent_to_sink_idx: 0,
            output_balancer_span: debug_span!(target: "services", parent: parent_span, "Output balancer"),
        }
    }

    #[instrument]
    pub async fn next_ready<'a>(&'a mut self) -> Permit<'a, T> {
        let size = self.sinks.len();
        let ordered_sinks = self.sinks[self.last_sent_to_sink_idx..]
            .iter()
            .chain(self.sinks[..self.last_sent_to_sink_idx].iter())
            .take(size)
            .enumerate();
        let wait_for_sink = info_span!(parent: &self.output_balancer_span, "Wait for available sink");
        match future::select_ok(ordered_sinks.map(|(i, sender)| {
            Box::pin(
                sender
                    .reserve()
                    .map(move |maybe_permit| maybe_permit.map(move |permit| (i, permit))),
            )
        }))
        .instrument(wait_for_sink)
        .await
        {
            Ok(((i, permit), _)) => {
                self.last_sent_to_sink_idx = i;
                permit
            }
            Err(error) => panic!("Reserving an output slot: {error}"),
        }
    }
}

impl<T> Debug for OutputBalancer<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("OutputBalancer")
            .field("sinks", &self.sinks)
            .field("last_sent_to_sink_idx", &self.last_sent_to_sink_idx)
            .finish()
    }
}
