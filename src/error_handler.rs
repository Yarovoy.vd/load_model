use std::{fmt::Debug, sync::Arc};

use tokio::sync::{
    mpsc::{self, Receiver, Sender},
    Mutex,
};
use tracing::{debug_span, info, instrument, Instrument};

pub struct ErrorHandler<T> {
    error_channel: Mutex<Receiver<T>>,
    error_span: tracing::Span,
}

impl<T> ErrorHandler<T> {
    #[instrument]
    pub fn new(error_channel: Receiver<T>, parent_span: &tracing::Span) -> Self {
        ErrorHandler {
            error_channel: Mutex::new(error_channel),
            error_span: debug_span!(target: "services", parent: parent_span, "Running error handler"),
        }
    }

    #[instrument]
    pub fn with_channel(parent_span: &tracing::Span) -> (Arc<Self>, Sender<T>) {
        let (sender, receiver) = mpsc::channel(1);
        let error_handler = Self::new(receiver, parent_span);
        (Arc::new(error_handler), sender)
    }
}

impl<T> ErrorHandler<T>
where
    T: Debug,
{
    #[instrument]
    pub async fn run(self: Arc<Self>) {
        let mut error_channel = self.error_channel.lock().await;
        loop {
            let error_span =
                debug_span!(target: "services", parent: &self.error_span, "Error handler round");
            async {
                let message = error_channel.recv().await;
                info!(target: "lifecycle_events", ?message, "Message was discarded");
            }
            .instrument(error_span)
            .await
        }
    }
}

impl<T> Debug for ErrorHandler<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ErrorHandler")
            .field("error_channel", &self.error_channel)
            .finish()
    }
}
