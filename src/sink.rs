use std::fmt::Debug;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

static SINK_ID_COUNTER: AtomicUsize = AtomicUsize::new(0);

fn next_sink_id() -> usize {
    SINK_ID_COUNTER.fetch_add(1, Ordering::Relaxed)
}
use std::{iter, time};

use tokio::sync::mpsc::{self, Receiver, Sender};
use tokio::sync::Mutex;
use tracing::{info, info_span, instrument, debug_span, Instrument};

pub struct ExponentialSink<T> {
    id: usize,
    delay_iter: Mutex<Box<dyn Iterator<Item = time::Duration> + Send>>,
    sink_channel: Mutex<Receiver<T>>,
    sink_span: tracing::Span,
}

impl<T> ExponentialSink<T> {
    #[instrument]
    pub fn new(
        mut start_delay: time::Duration,
        delay_coeff: f64,
        sink_channel: Receiver<T>,
        parent_span: &tracing::Span,
    ) -> Self {
        let delay_iter = Box::new(iter::from_fn(move || {
            start_delay = start_delay.mul_f64(delay_coeff);
            Some(start_delay)
        }));
        let id = next_sink_id();
        ExponentialSink {
            id,
            delay_iter: Mutex::new(delay_iter),
            sink_channel: Mutex::new(sink_channel),
            sink_span: debug_span!(target: "services", parent: parent_span, "Sink", sink_id = id),
        }
    }

    #[instrument]
    pub fn with_channel(start_delay: time::Duration, delay_coeff: f64, parent_span: &tracing::Span) -> (Arc<Self>, Sender<T>) {
        let (sender, receiver) = mpsc::channel(1);
        let sink = Self::new(start_delay, delay_coeff, receiver, parent_span);
        (Arc::new(sink), sender)
    }
}

impl<T> ExponentialSink<T>
where
    T: Debug,
{
    #[instrument]
    pub async fn run(self: Arc<Self>) {
        let sink_round = info_span!(parent: &self.sink_span, "Sink round", sink_id = self.id);
        let mut sink_channel = self.sink_channel.lock().await;
        let mut delay_stream = self.delay_iter.lock().await;
        for delay in delay_stream.as_mut() {
            let sink_sleep = info_span!(target: "services", parent: &sink_round, "Sink sleeping", sink_id = self.id);
            tokio::time::sleep(delay).instrument(sink_sleep).await;
            let sink_wait_message = info_span!(target: "lifecycle_events", parent: &sink_round, "Sink waiting for next message", sink_id = self.id);
            if let Some(message) = sink_channel.recv().instrument(sink_wait_message).await {
                info!(target: "lifecycle_events", ?message, sink_id = self.id, "Received into sink");
            } else {
                let self_id = self.id;
                panic!("Sink channel for sink {self_id} was closed")
            }
        }
        let self_id = self.id;
        panic!("Delay iterator was exhausted for sink {self_id}");
    }
}

impl<T> Debug for ExponentialSink<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ExponentialSink")
            .field("id", &self.id)
            .field("sink_channel", &self.sink_channel)
            .finish()
    }
}
