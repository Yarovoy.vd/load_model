use std::fmt::Debug;
use std::iter;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

static GENERATOR_ID_COUNTER: AtomicUsize = AtomicUsize::new(0);

fn next_generator_id() -> usize {
    GENERATOR_ID_COUNTER.fetch_add(1, Ordering::Relaxed)
}

use std::time::Duration;

use tokio::sync::mpsc::{self, Receiver, Sender};
use tokio::sync::Mutex;
use tokio::time::{interval, Interval};
use tracing::{debug_span, info, instrument, Instrument, info_span};
use crate::message::GeneratorIter;

pub struct SteadyGenerator<T> {
    id: usize,
    timer: Mutex<Interval>,
    generator: Mutex<GeneratorIter<T>>,
    output: Sender<T>,
    generator_span: tracing::Span,
}

impl<T> SteadyGenerator<T> {
    #[instrument(skip(generator))]
    pub fn new(
        period: Duration,
        generator: GeneratorIter<T>,
        output: Sender<T>,
        parent_span: &tracing::Span,
    ) -> Self {
        let id = next_generator_id();
        SteadyGenerator {
            id,
            timer: Mutex::new(interval(period)),
            generator: Mutex::new(generator),
            output,
            generator_span: debug_span!(target: "services", parent: parent_span, "Generator", gen_id = id),
        }
    }

    #[instrument(skip(generator))]
    pub fn with_channel(
        period: Duration,
        generator: GeneratorIter<T>,
        parent_span: &tracing::Span,
    ) -> (Arc<Self>, Receiver<T>) {
        let (sender, receiver) = mpsc::channel(1);
        let generator = Self::new(period, generator, sender, parent_span);
        (Arc::new(generator), receiver)
    }
}

impl<T> SteadyGenerator<T>
where
    T: Debug + Clone,
{
    #[instrument(fields(gen_id = self.id))]
    pub async fn run(self: Arc<Self>) {
        let generator_round = info_span!(parent: &self.generator_span, "Generator round", gen_id = self.id);
        let mut gen_stream = self.generator.lock().await;
        let mut ticker = self.timer.lock().await;
        for value in gen_stream.as_mut() {
            let generator_sleep = debug_span!(target: "services", parent: &generator_round, "Generator sleeping", gen_id = self.id);
            ticker.tick().instrument(generator_sleep).await;
            let generator_wait_message = info_span!(target: "lifecycle_events", parent: &generator_round, "Generator waiting to push next message", gen_id = self.id);
            if let Err(send_error) = self.output.send(value.clone()).instrument(generator_wait_message).await {
                let generator_id = self.id.clone();
                panic!("Generator [Steady {generator_id}] has experienced an error {send_error}")
            } else {
                info!(target: "lifecycle_events", message = ?value, gen_id = self.id, "Sent generated message");
            }
        }
        let generator_id = self.id.clone();
        panic!("Generator value stream has ended (id {generator_id})");
    }
}

impl<T> Debug for SteadyGenerator<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SteadyGenerator")
            .field("id", &self.id)
            .field("timer", &self.timer)
            .field("output", &self.output)
            .finish()
    }
}

#[cfg(test)]
mod test {
    use std::time::Duration;

    use spectral::prelude::*;
    use tracing::info_span;
    use crate::message::generator_counter;

    #[tokio::test]
    async fn generator_sends_to_buffered_channel_ten_times() {
        let test_span = info_span!("test generator_sends_to_buffered_channel_ten_times");
        let period = Duration::from_millis(3);
        let gen_vals = generator_counter();
        let (generator, mut receiver) = super::SteadyGenerator::with_channel(period, gen_vals, &test_span);
        let gen_task = tokio::spawn(generator.run());

        for i in 1..11 {
            let from_gen = receiver.recv().await;
            assert_that(&from_gen).is_some().is_equal_to(i);
        }

        gen_task.abort();
        let gen_res = gen_task.await;
        assert_that(&gen_res)
            .is_err()
            .matches(|error| error.is_cancelled())
    }
}
